# Nadjib Achir
# CP2I
import time
import grovepi
import math

sensor = 4
blue = 0

while True:
    try:
        [temp, humidity] = grovepi.dht(sensor, blue)

        if not math.isnan(temp) and not math.isnan(humidity):
            print("Temp = %.02fc - Humidity = %.02f%%" % (temp, humidity))
        
        time.sleep(2)
    except IOError:
        print("IOError")
    except KeyboardInterrupt:
        print("KeyboardInterrupt")

