# Nadjib Achir
# CP2I
import time
import grovepi
 
# Connect the Grove LED to digital port D4
led = 4
grovepi.pinMode(led,"OUTPUT") # connect the led sensor
time.sleep(1)
 
print ("This example will blink a Grove LED ")
print (" connected to the GrovePi+ on the port")
print ("labeled D4.")
print (" ")
print ("Connect the LED to the port labele D4!" )
 
while True:
    try:
        # turn the LED to ON
        grovepi.digitalWrite(led,1)     
        print ("LED ON!")
        time.sleep(1) # wait 1 second

        # turn the LED to ON
        grovepi.digitalWrite(led,0)
        print ("LED OFF!")
        time.sleep(1) # wait 1 second
 
    except KeyboardInterrupt:
        # Turn LED off before stopping   
        grovepi.digitalWrite(led,0)
        break
    except IOError:
        # Print "Error" if communication 
        # error encountered
        print ("Error")
