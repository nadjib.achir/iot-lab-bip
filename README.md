# Lab-IoT

## Objective

This lab aims to gain familiarity with the software and hardware components required to establish an IoT platform. By completing this lab, participants will comprehensively understand the essential elements for building and configuring an IoT infrastructure. This includes becoming acquainted with the software tools and frameworks employed for IoT development and the hardware components utilized in constructing the platform. Through hands-on exercises and demonstrations, participants will gain practical knowledge and expertise in setting up an effective IoT environment.

